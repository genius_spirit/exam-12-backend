const express = require("express");
const multer = require("multer");
const path = require("path");
const nanoid = require("nanoid");
const Photo = require("../models/Photo");

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const createRouter = () => {
const router = express.Router();

  router.get("/", async(req, res) => {

    const photos = await Photo.find().populate('author');

    try {
      if (photos) res.send(photos);
    } catch (error) {
      return res.status(500).send({message: error})
    }
  });


  router.post("/", [auth, upload.single("photo")], async(req, res) => {
    const data = req.body;

    const message = 'Fields title and photo can not be blank';
    if (req.body.title === '') res.status(400).send({message: message});

    if (req.file) {
      data.photo = req.file.filename;
    } else res.status(400).send({message: message});

    data.author = req.user._id;

    const newPhoto = new Photo(data);

    const photo = await newPhoto.save();
    try {
      if (photo) res.send(photo);
    } catch (e) {
      res.status(400).send(error);
    }
  });

  router.get("/:id", async(req, res) => {
    const id = req.params.id;
    const photos = await Photo.find({author: id}).populate('author');
    try {
      if (photos) res.send(photos);
      else res.status(404).send({message: 'You have not saved photos'})
    } catch (error) {
      return res.status(500).send({message: error})
    }
  });

  router.delete("/:id", auth, async(req, res) => {
    const photo = await Photo.deleteOne({ _id: req.params.id });
    try {
      if (photo) res.send(photo);
    } catch(error) {
      return res.send({message: error});
    }
  });


  return router;
};



module.exports = createRouter;
