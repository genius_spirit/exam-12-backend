const mongoose = require("mongoose");
const config = require("./config");
const Photo = require("./models/Photo");
const User = require("./models/User");

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

const collections = ["photos", "users"];

db.once("open", async () => {
  collections.forEach(async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });

  const [user1, user2] = await User.create(
    {
      username: "user1",
      password: "123",
      displayName: "John Dow"
    },
    {
      username: "user2",
      password: "123",
      displayName: "Jack Black"
    },
    {
      username: "admin",
      password: "admin123",
      role: "admin",
      displayName: "Admin Admin"
    }
  );

  await Photo.create(
    {
      title: "italy-laundry-washing-venice",
      photo: "photo-1.jpeg",
      author: user1._id
    },
    {
      title: "assorted-fruits-on-white-ceramic-plate",
      photo: "photo-2.jpeg",
      author: user1._id
    },
    {
      title: "colleagues-cooperation-fist-bump-fists",
      photo: "photo-3.jpeg",
      author: user1._id
    },
    {
      title: "people-taking-pictures-of-vintage-cameras",
      photo: "photo-4.jpeg",
      author: user1._id
    },
    {
      title: "green-rice-field",
      photo: "photo-5.jpeg",
      author: user2._id
    },
    {
      title: "clouds-dawn-dusk-forest",
      photo: "photo-6.jpeg",
      author: user2._id
    },
    {
      title: "low-angle-photography-of-airplane-flying-above-high-rise-buildings",
      photo: "photo-7.jpeg",
      author: user2._id
    },
    {
      title: "architecture-bridge-buildings-business",
      photo: "photo-8.jpeg",
      author: user2._id
    }
  );

  db.close();
});
